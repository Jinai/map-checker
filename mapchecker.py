# -*- coding: utf-8 -*-
# !python3

# standard
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.font as tkFont
import tkinter.messagebox as mbox
import tkinter.filedialog as fdialog
import webbrowser
import threading
import datetime
import codecs
import queue
import sys
import os

# PyPI
import pyperclip
import requests

# non-standard
import playlist
import logger  # custom logger widget

requests.packages.urllib3.disable_warnings()

# ---------------------- INIT ---------------------- #

LINKS = ("https://www.virtualbox.be/public.php?service=files&t=b1adfaf499545771bb06b9dcc38d0520&download",
         "http://jinai.be/master_playlist.txt",
         "http://playlist-manager.olympe.in/master_playlist.txt")
HEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/20.0'}
LINE_BREAK = "\n"

VERSION = "1.2"
ICON_DATA = b'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAuUlEQVQ4jZWTYRGDMAxGHzMCDiqBOdgcIGESJgEJlbA5mAQklCnoHLA/6TUrLV2/u1xzJV/oo6Ejrx4YJF+Bd6EuqxHYkhhbGjgxLSp3gJV80sVdpsEm6wB8JLSusrdSQPPSxAL3BMWp566EMLP/Bmm4FCWHkTPNgDkywu8teEG51ExagfvxT/Eps3eW9dny1qCJePwqa6owKCFuLWajjC8q91zSohqEYWn6B4wYPXGYbOspgnoi0qG+Iw9LoQcUz5IAAAAASUVORK5CYII='
# BG_COLOR = "#303036"
BG_COLOR = "white"
BUTTON_WIDTH = 10

example_links = "http://www.example.com/"
example_code = "@1352562682151"  # tiinx 1, Tiinx, medium

# -------------------------------------------------- #

"""
TODO :
- Append logs with session id
- Help Toplevel
- Remember last visited dir in .ini file (configparser)
- 
- 
"""


class FetchData(threading.Thread):
    def __init__(self, queue, logger, links, headers=None, timeout=10):
        threading.Thread.__init__(self)
        self.queue = queue
        self.logger = logger
        self.links = links
        self.headers = headers if headers else {}
        self.timeout = timeout

    def run(self):
        for link in self.links:
            try:
                text = requests.get(link, headers=self.headers, timeout=self.timeout, verify=False)
            except requests.exceptions.ConnectionError as e:
                self.logger.insert("Erreur de connexion\n" + str(e))
                if link != self.links[-1]:
                    self.logger.insert("Retrying...")
            except requests.exceptions.Timeout as e:
                self.logger.insert("Timeout\n" + str(e))
                if link != self.links[-1]:
                    self.logger.insert("Retrying...")
            else:
                self.queue.put(text)
                break
        if self.queue.empty():
            self.queue.put(None)


class MapChecker(tk.Frame):
    def __init__(self, master, links, line_break, headers=None, **opts):
        tk.Frame.__init__(self, master, **opts)
        self.master = master
        self.links = links
        self.line_break = line_break
        self.headers = headers if headers else {}
        self.master_playlist = None
        self.user_playlist = None
        self.wrong_maps = None
        self.queue = queue.Queue()
        self.font = tkFont.Font(family='arial', size=8, weight='normal')
        self.initialdir = os.path.normpath(os.path.expanduser('~/Desktop/'))
        self.setup_widgets()

    def setup_widgets(self):
        # label/entry/button combo
        frame_check = ttk.Frame(self)
        frame_check.pack(fill='x')
        self.label = ttk.Label(frame_check, text="Code : ")
        self.label.pack(side='left')
        self.entry = ttk.Entry(frame_check)
        self.entry.pack(side='left', fill='x', expand=True, padx=5)
        self.button_check = ttk.Button(frame_check, text='Check', command=self.check)
        self.button_check.pack(side='right')

        # logger widget
        initialdir = os.path.normpath(os.path.expanduser('~/Desktop/'))
        dialog = {'defaultextension': '.log', 'initialfile': 'mapchecker', 'initialdir': initialdir}
        font = tkFont.Font(family='arial', size=8, weight='normal')
        self.logger = logger.Logger(self, font, 55, 10, tk.WORD, dialog=dialog, bg_color='white')
        self.logger.pack(fill='both', expand=True, pady=5)
        self.entry.focus_set()

        # options : clipboard, clear/save
        frame_options = ttk.Frame(self)
        frame_options.pack(fill='x')
        self.cbool = tk.BooleanVar(0)
        clipboard_bool = ttk.Checkbutton(frame_options, text='Copier la map si elle existe', variable=self.cbool,
                                         offvalue=0, onvalue=1)
        clipboard_bool.state(['!alternate'])
        clipboard_bool.pack(side='left')
        self.button_clear = ttk.Button(frame_options, text="Clear logs", command=self.logger.clear)
        self.button_clear.pack(side='right')
        self.button_save = ttk.Button(frame_options, text="Save logs", command=lambda: self.logger.save(True))
        self.button_save.pack(side='right')

        # bindings
        self.entry.bind('<Return>', lambda e: self.button_check.invoke())
        self.update_idletasks()

    def log(self, msg):
        self.logger.insert(msg)

    def check(self):
        if self.entry.get():
            if not self.master_playlist:
                FetchData(self.queue, self.logger, self.links, self.headers).start()
                self.master.after(100, lambda: self.get_queue(self.compare_input))
            else:
                self.compare_input()

    def get_queue(self, callback=lambda *x: None):
        try:
            data = self.queue.get(block=False)
            if data:
                pl = playlist.Playlist()
                pl.from_string(data.text, self.line_break)
                self.master_playlist = pl
                callback()
        except queue.Empty:
            self.master.after(100, lambda: self.get_queue(callback))

    def compare_input(self):
        code = self.entry.get()
        to_clipboard = self.cbool.get()
        if code.startswith('@') and len(code) >= 14 and code[1:14].isdigit():
            found = False
            for i, m in enumerate(self.master_playlist.original):
                if code[:14] in str(m):
                    found = True
                    self.logger.insert("Doublon : {} [Ligne {}]".format(str(m), str(i + 1)))
                    if to_clipboard:
                        pyperclip.copy(str(m))
                        self.logger.insert("Map copiée dans le presse-papiers")
                    break
            if not found:
                self.logger.insert("Unique : {0}".format(code))

    def open(self):
        name = fdialog.askopenfilename(filetypes=(("Text Files", "*.txt"), ("All Files", "*.*")),
                                       initialdir=self.initialdir)
        if name:
            self.logger.insert("Comparaison avec : {}".format(name))
            errors = []
            try:
                f = open(name, 'r', encoding='utf-8')
                self.user_playlist = playlist.Playlist()
                for line in f:
                    line = line.strip()
                    try:
                        self.user_playlist.from_string(line, self.line_break)
                    except ValueError as e:
                        errors.append((line, str(e)))
                    except TypeError as e:
                        errors.append((line, "Erreur inconnue (probablement un problème de virgules)"))
            except IOError as e:
                self.logger.insert(str(e))
            else:
                f.close()
                self.wrong_maps = errors
                if not self.master_playlist:
                    FetchData(self.queue, self.logger, self.links, self.headers).start()
                    self.master.after(100, lambda: self.get_queue(lambda: self.compare_file()))
                else:
                    self.compare_file()

    def compare_file(self):
        result = ''
        doublons = []
        new = []
        for m in self.user_playlist.original:
            if m in self.master_playlist.original:
                doublons.append(m)
            else:
                new.append(m)
        ts = datetime.datetime.today().strftime("%Y-%m-%d à %H:%M:%S")
        msg = "Résultat : {} doublons, {} uniques, {} erreurs".format(len(doublons), len(new), len(self.wrong_maps))
        result += "Rapport généré le {}\n{}\nDétails :\n\n".format(ts, msg)
        result += "MAPS DOUBLONS\n=============\n\n"
        result += "\n".join([str(m) for m in doublons])
        result += "\n\n\nMAPS UNIQUES\n============\n\n"
        result += "\n".join([str(m) for m in new])
        result += "\n\n\nERREURS\n=======\n\n"
        result += "\n".join([m[0] + '\n' + m[1] for m in self.wrong_maps])
        self.logger.insert(msg)
        del self.wrong_maps[:]
        name = fdialog.asksaveasfilename(defaultextension='.txt', initialfile='rapport', initialdir=self.initialdir)
        if name:
            try:
                f = open(name, 'w', encoding='utf-8')
                f.write(result)
            except IOError as e:
                self.logger.insert(str(e))
            else:
                f.close()
                self.logger.insert("Rapport : {}".format(name))
                os.startfile(name)
            #self.master.after(200, lambda:self.lift(self.master, self.entry))

    def get_latest(self):
        name = fdialog.asksaveasfilename(defaultextension='.txt', initialfile='master_playlist',
                                         initialdir=self.initialdir)
        if name:
            FetchData(self.queue, self.logger, self.links, self.headers).start()
            self.master.after(100, lambda: self.get_queue(lambda: self.write(name)))

    def write(self, name):
        try:
            f = open(name, 'w', encoding='utf-8')
            f.write(self.master_playlist.__str__())
        except IOError as e:
            self.logger(str(e))
        else:
            self.logger.insert("Playlist : {0}".format(name))
            f.close()

    def lift(self, window, to_focus=None):
        window.attributes('-topmost', 1)
        window.attributes('-topmost', 0)
        if to_focus is not None:
            to_focus.focus_force()

    def quit(self):
        raise SystemExit

    def help(self):
        mbox.showinfo("Help", "En construction...")

    def about(self):
        about = tk.Toplevel(pady=5, padx=5)
        about.title("About")
        about.resizable(0, 0)
        notebook = ttk.Notebook(about, takefocus=False)
        notebook.pack()
        frame1 = ttk.Frame(notebook, takefocus=False)
        frame1.pack()
        text = ttk.Label(frame1,
                         text="Un grand merci à :\n    - Liroparabe\n    - Garganta\n    - Yiuye\n    - Hueco\npour le beta-testing !")
        text.pack()
        text2 = ttk.Label(about, text="Codé en python par Jinai")
        text2.pack()
        secret = ttk.Frame(notebook, takefocus=False)
        secret.pack(fill='both', expand='True')
        button = ttk.Button(secret, takefocus=False, command=self.easter_egg)
        button.pack(fill='both', expand=True)
        notebook.add(frame1, text='Remerciements')
        notebook.add(secret)
        about.focus_set()

    def easter_egg(self):
        try:
            #path = os.path.join(os.getcwd(), 'tk/ttk/00D7F048B53ABDE553E05D4A1226E1D9.mp3')
            webbrowser.open("http://playlist-manager.olympe.in/00D7F048B53ABDE553E05D4A1226E1D9.mp3")
            pass
        except IOError as e:
            pass
        finally:
            secret = tk.Toplevel(pady=5, padx=5)
            secret.title('???????')
            secret_lframe = ttk.LabelFrame(secret, text='Secret')
            secret_lframe.pack()
            secret_text = ttk.Label(secret_lframe, text="Si tu lis ceci, propose 20 maps d'ici une semaine ou tu vivras le restant de tes jours \
avec le cerveau de Deab ou en colloc avec Aaajulienctr.", wraplength=200, justify='left')
            secret_text.pack()
            self.master.after(200, lambda: self.lift(secret))


if __name__ == '__main__':
    root = tk.Tk()
    root.title("Map Checker v" + VERSION)
    root.tk.call('encoding', 'system', 'utf-8')
    icon = tk.PhotoImage(data=ICON_DATA)
    root.tk.call('wm', 'iconphoto', root._w, icon)

    mchk = MapChecker(root, LINKS, LINE_BREAK, HEADERS)
    mchk.pack(side='top', pady=5, padx=5, fill='both', expand=True)

    # menu options
    menubar = tk.Menu(mchk)
    root.config(menu=menubar)
    filemenu = tk.Menu(menubar, tearoff=0)
    helpmenu = tk.Menu(menubar, tearoff=0)
    menubar.add_cascade(label="File", menu=filemenu)
    menubar.add_cascade(label="Help", menu=helpmenu)
    filemenu.add_command(label="Comparer...", accelerator="Ctrl+O", command=mchk.open)
    filemenu.add_command(label="Download playlist", accelerator="Ctrl+P", command=mchk.get_latest)
    filemenu.add_separator()
    filemenu.add_command(label="Quit", accelerator="Ctrl+Q", command=mchk.quit)
    helpmenu.add_command(label="Interface", accelerator="Ctrl+I", command=mchk.help)
    helpmenu.add_command(label="About", accelerator="Ctrl+B", command=mchk.about)


    # bindings
    root.minsize(mchk.winfo_width(), mchk.winfo_height())
    root.bind('<Control-o>', lambda event: mchk.open())
    root.bind('<Control-p>', lambda event: mchk.get_latest())
    root.bind('<Control-q>', lambda event: mchk.quit())
    root.bind('<Control-i>', lambda event: mchk.help())
    root.bind('<Control-b>', lambda event: mchk.about())

    root.mainloop()