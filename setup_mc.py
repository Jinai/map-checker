# -*- coding: utf-8 -*-
# !python3

import sys
import importlib

importlib.reload(sys)
if hasattr(sys, "setdefaultencoding"):
    sys.setdefaultencoding("utf-8")


def py2exe():
    from distutils.core import setup
    import py2exe

    includes = ["encodings", "encodings.utf_8", ]

    setup(
        options={"py2exe": {"compressed": True,
                            "optimize": 2,
                            "bundle_files": 2,
                            "dist_dir": "dist_py2exe",
                            "includes": includes,
        }
        },
        zipfile=None,
        windows=[{"script": "mapchecker.py",
                  "icon_resources": [(0, "resources/aaaah.ico")]
                 }],
        data_files=[('resources', ['resources/aaaah.txt', 'resources/aaaah.ico']), ('', ['mapchecker_p2x.iss'])]
    )


def cx_freeze():
    from cx_Freeze import setup, Executable

    include_files = [('resources/aaaah.ico', 'resources/aaaah.ico'),
                     ('resources/aaaah.txt', 'resources/aaaah.txt'),
                     'mapchecker_cx.iss']

    options = {"create_shared_zip": False,
               "append_script_to_exe": True,
               "include_in_shared_zip": False,
               "include_files": include_files,
               "optimize": 0  # "include_msvcr": True,
    }

    target = Executable(
        script="mapchecker.py",
        base="Win32GUI",
        icon='resources/aaaah.ico',
        compress=True)

    setup(
        name="Map Checker",
        version="1.2",
        description="A playlist comparison tool for the Playlist Project",
        author="Jinai",
        author_email="jinai.extinction@gmail.com",
        maintainer="Jinai",
        maintainer_email="jinai.extinction@gmail.com",
        options={"build_exe": options},
        executables=[target]
    )


def usage():
    print("Usage : python " + sys.argv[0] + " py2exe|cx_Freeze")


if __name__ == '__main__':
    arg = sys.argv[-1]
    if arg == 'py2exe':
        py2exe()
    elif arg == 'build' or arg == 'bdist_msi':
        cx_freeze()
    else:
        usage()