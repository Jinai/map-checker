# -*- coding: utf-8 -*-
# !python3

from datetime import datetime
from enum import Enum
from functools import reduce
from random import shuffle
import itertools
import operator


class Level(Enum):
    Easy = 1
    Medium = 2
    Hard = 3
    Nc = 4


class Map():
    @classmethod
    def is_valid(cls, code):
        return code.startswith('@') and len(code) == 14 and code[1:].isdigit()

    def __init__(self, code,
                 titre='', auteur='', level='', comment='',
                 definite_level=True):
        if not Map.is_valid(code):
            raise ValueError(
                "{} : Le code n'a pas le bon format (doit commencer par @ et avoir "
                "13 chiffres)".format(code))
        try:
            self.level = Level[level.capitalize()]
        except KeyError:
            raise ValueError(
                "Seuls les niveaux 'easy', 'medium' et 'hard' sont autorisés.")
        self.code = code
        self.titre = titre
        self.auteur = auteur
        self.comment = comment
        self.definite_level = definite_level
        self.id = int(code[1:])

    def timestamp(self):
        return int(self.code[1:-3])

    def datetime(self):
        return datetime.fromtimestamp(self.timestamp()).strftime(
            '%d-%m-%Y %H:%M:%S')

    def __eq__(self, other):
        return self.id == other.id

    def __hash__(self):
        return self.id

    def __str__(self):
        line = "{0}, {1}, {2}, {3}".format(self.code, self.titre, self.auteur,
                                           self.level.name)
        if not self.definite_level:
            line += " (?)"
        if self.comment:
            line += " //" + self.comment
        return line.strip()

    def __repr__(self):
        return self.__str__()


class Playlist():
    COM_SEP = "//"
    Q_MARK = "(?)"

    def __init__(self, original=None, easy=None, medium=None, hard=None,
                 nc=None, duplicates=None):
        self.original = original if original else []  # à améliorer
        self.easy = easy if easy else []
        self.medium = medium if medium else []
        self.hard = hard if hard else []
        self.nc = nc if nc else []
        self.duplicates = duplicates if duplicates else {}

        self._path = None
        self._maps = {
            Level.Easy: self.easy,
            Level.Medium: self.medium,
            Level.Hard: self.hard,
            Level.Nc: self.nc,
        }

    def open(self, filename):
        self._path = filename
        with open(filename, 'r', encoding='utf-8') as f:
            for line in f:
                self.from_string(line)

    def from_string(self, text, newline='\n'):
        for line in text.split(newline):
            line = line.strip()
            if ';' in line:
                raise ValueError(
                    "Chaque entrée est séparée d'un ',' et non d'un ';'")
            if len(line) >= 14:  # petit hack pour les newline en fin de fichier
                comment = ''
                pos = line.find(Playlist.COM_SEP)  # "//"
                comsep = True if pos != -1 else False
                if comsep:
                    line, comment = line[:pos], line[pos + 2:]

                pos = line.find(Playlist.Q_MARK)  # "(?)"
                qmark = True if pos != -1 else False
                if qmark:
                    line = line[:pos]
                args = [arg.strip() for arg in line.split(',')]
                m = Map(*args, comment=comment, definite_level=not qmark)
                self.add(m)

    def add(self, map):
        if map in self:
            if map in self.duplicates:
                self.duplicates[map] += 1
            else:
                self.duplicates[map] = 1
            return False
        else:
            self.original.append(map)
            self._maps[map.level].append(map)
            return True

    def remove(self, map):
        if map in self:
            self._maps[map.level].remove(map)
            try:
                del self.duplicates[map]
            except:
                pass

    def generate_playlist(self, amount=200, e_coef=1 / 3, m_coef=1 / 3,
                          h_coef=1 / 3, random=False):
        if random:
            p = Playlist()
            if amount <= len(self):
                all = list(self.all())
                shuffle(all)
                p.original = all[:amount]
            return p
        else:
            easy = int(amount * e_coef)
            medium = int(amount * m_coef)
            hard = amount - (easy + medium)
            shuffle(self.easy)
            shuffle(self.medium)
            shuffle(self.hard)
            return Playlist(
                self.easy[:easy] + self.medium[:medium] + self.hard[:hard])

    def duplicate_count(self):
        return reduce(operator.add, self.duplicates.values(), 0)

    def all_count(self):
        return len(self.easy), len(self.medium), len(self.hard)

    def all(self):
        return itertools.chain(self.easy, self.medium, self.hard)

    def info(self):
        return len(self.easy), len(self.medium), len(self.hard), self.duplicate_count(), len(self.original)

    def __sub__(self, other):
        p = Playlist()
        p.from_string("\n".join(
            [str(m) for m in self.original if m not in other.original]))
        return p

    def __len__(self):
        return len(self.easy) + len(self.medium) + len(self.hard)

    def __contains__(self, key):
        for map in self.all():
            if key.id == map.id:
                return True
        return False

    def __str__(self):
        return "\n".join([str(m) for m in self.original])

    def __repr__(self):
        return "<{0} easy, {1} medium, {2} hard, {3} duplicates, {4} total>".format(*self.info())


if __name__ == '__main__':
    path = "playlists/master_playlist.txt"
    path2 = "playlists/master_playlist2.txt"
    p = Playlist()
    p.open(path)
    p2 = Playlist()
    # p2.open(path2)
    # print(p-p2)
    # print(p)
    print(repr(p))
    print("doublons:", p.duplicates)
